# Norsk tippeprogram for fotballtipping #

![xpert.jpg](https://bitbucket.org/repo/MyznoB/images/2462576609-xpert.jpg)

Kun for demonstrasjon

## Funksjon(er): ##
* Oppsett av en eller flere systemer
* Hente og lagre system (.xpf-filer)
* Sett opp stamme, blokker, utgangsrekker, tegnfordelinger og maks på rad
* Oppsett av lag
* Lagring av online format (NTF-format) for levering
* Premiekontroll
* Visning av genererte rekker

Teknologier benyttet: Visual Studio 2010, C#, Windows Forms, .Net Framework 2.0