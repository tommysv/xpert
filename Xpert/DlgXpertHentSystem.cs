﻿using System;
using System.IO;
using System.Windows.Forms;

using Xpert.Modell.Objekter;

namespace Xpert
{
    public partial class DlgXpertHentSystem : Form
    {
        string dataKatalog;
        XpertSystemType systemType;

        public DlgXpertHentSystem()
        {
            InitializeComponent();
            dataKatalog = XpertKonfigurasjon.KatalogXpertData;
            lblKatalog.Text = dataKatalog;
            if (!Directory.Exists(dataKatalog))
                Directory.CreateDirectory(dataKatalog);
        }

        private void PopulerListbox(string ext)
        {
            string[] filer = Directory.GetFiles(dataKatalog, ext, SearchOption.TopDirectoryOnly);
            for (int nFil = 0; nFil < filer.Length; nFil++)
            {
                lbxSystemer.Items.Add(Path.GetFileName(filer[nFil]));
            }
        }

        public XpertSystemType SystemType
        {
            get
            {
                return systemType;
            }
        }

        public string Filnavn
        {
            get
            {
                return Path.Combine(dataKatalog, Path.ChangeExtension(txtFilnavn.Text, "xpf"));
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            cbxFiltype.SelectedIndex = 0;
        }

        private void lbxSystemer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbxSystemer.SelectedIndex != -1)
            {
                txtFilnavn.Text = Convert.ToString(lbxSystemer.SelectedItem);
            }
        }

        private void cbxFiltype_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbxFiltype.SelectedIndex != -1)
            {
                switch (cbxFiltype.SelectedIndex)
                {
                    case 0: 
                        PopulerListbox("*.xpf");
                        systemType = XpertSystemType.Fotball;
                        break;
                }
            }
        }
    }
}
