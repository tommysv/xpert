﻿namespace Xpert
{
    partial class DlgXpertFotballRekker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DlgXpertFotballRekker));
            this.uscKupong = new Xpert.Kontroller.Tipping.UscXpertTippingKupong();
            this.btnBlaFørsteKupong = new System.Windows.Forms.Button();
            this.btnBlaForrigeKupong = new System.Windows.Forms.Button();
            this.btnBlaSisteKupong = new System.Windows.Forms.Button();
            this.btnBlaNesteKupong = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // uscKupong
            // 
            this.uscKupong.BackColor = System.Drawing.Color.White;
            this.uscKupong.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("uscKupong.BackgroundImage")));
            this.uscKupong.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.uscKupong.Location = new System.Drawing.Point(0, 0);
            this.uscKupong.Name = "uscKupong";
            this.uscKupong.Rekker = null;
            this.uscKupong.Size = new System.Drawing.Size(590, 265);
            this.uscKupong.TabIndex = 0;
            // 
            // btnBlaFørsteKupong
            // 
            this.btnBlaFørsteKupong.Location = new System.Drawing.Point(132, 271);
            this.btnBlaFørsteKupong.Name = "btnBlaFørsteKupong";
            this.btnBlaFørsteKupong.Size = new System.Drawing.Size(30, 23);
            this.btnBlaFørsteKupong.TabIndex = 1;
            this.btnBlaFørsteKupong.Text = "<<";
            this.btnBlaFørsteKupong.UseVisualStyleBackColor = true;
            this.btnBlaFørsteKupong.Click += new System.EventHandler(this.btnBlaFørsteKupong_Click);
            // 
            // btnBlaForrigeKupong
            // 
            this.btnBlaForrigeKupong.Location = new System.Drawing.Point(190, 271);
            this.btnBlaForrigeKupong.Name = "btnBlaForrigeKupong";
            this.btnBlaForrigeKupong.Size = new System.Drawing.Size(30, 23);
            this.btnBlaForrigeKupong.TabIndex = 2;
            this.btnBlaForrigeKupong.Text = "<";
            this.btnBlaForrigeKupong.UseVisualStyleBackColor = true;
            this.btnBlaForrigeKupong.Click += new System.EventHandler(this.btnBlaForrigeKupong_Click);
            // 
            // btnBlaSisteKupong
            // 
            this.btnBlaSisteKupong.Location = new System.Drawing.Point(427, 271);
            this.btnBlaSisteKupong.Name = "btnBlaSisteKupong";
            this.btnBlaSisteKupong.Size = new System.Drawing.Size(30, 23);
            this.btnBlaSisteKupong.TabIndex = 4;
            this.btnBlaSisteKupong.Text = ">>";
            this.btnBlaSisteKupong.UseVisualStyleBackColor = true;
            this.btnBlaSisteKupong.Click += new System.EventHandler(this.btnBlaSisteKupong_Click);
            // 
            // btnBlaNesteKupong
            // 
            this.btnBlaNesteKupong.Location = new System.Drawing.Point(368, 271);
            this.btnBlaNesteKupong.Name = "btnBlaNesteKupong";
            this.btnBlaNesteKupong.Size = new System.Drawing.Size(30, 23);
            this.btnBlaNesteKupong.TabIndex = 3;
            this.btnBlaNesteKupong.Text = ">";
            this.btnBlaNesteKupong.UseVisualStyleBackColor = true;
            this.btnBlaNesteKupong.Click += new System.EventHandler(this.btnBlaNesteKupong_Click);
            // 
            // DlgXpertFotballRekker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(592, 295);
            this.Controls.Add(this.btnBlaSisteKupong);
            this.Controls.Add(this.btnBlaNesteKupong);
            this.Controls.Add(this.btnBlaForrigeKupong);
            this.Controls.Add(this.btnBlaFørsteKupong);
            this.Controls.Add(this.uscKupong);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DlgXpertFotballRekker";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Rekkevisning";
            this.ResumeLayout(false);

        }

        #endregion

        private Xpert.Kontroller.Tipping.UscXpertTippingKupong uscKupong;
        private System.Windows.Forms.Button btnBlaFørsteKupong;
        private System.Windows.Forms.Button btnBlaForrigeKupong;
        private System.Windows.Forms.Button btnBlaSisteKupong;
        private System.Windows.Forms.Button btnBlaNesteKupong;

    }
}