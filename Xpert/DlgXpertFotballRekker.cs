﻿using System;
using System.Windows.Forms;

using Xpert.Modell.Objekter.Tipping;
using Xpert.Kontroller.Tipping;

namespace Xpert
{
    public partial class DlgXpertFotballRekker : Form
    {
        public DlgXpertFotballRekker()
        {
            InitializeComponent();
        }

        public XpertTippingRekker Rekker
        {
            get
            {
                return uscKupong.Rekker;
            }
            set
            {
                uscKupong.Rekker = value;
            }
        }

        private void btnBlaFørsteKupong_Click(object sender, EventArgs e)
        {
            uscKupong.BlaFørsteKupong();
        }

        private void btnBlaForrigeKupong_Click(object sender, EventArgs e)
        {
            uscKupong.BlaForrigeKupong();
        }

        private void btnBlaNesteKupong_Click(object sender, EventArgs e)
        {
            uscKupong.BlaNesteKupong();
        }

        private void btnBlaSisteKupong_Click(object sender, EventArgs e)
        {
            uscKupong.BlaSisteKupong();
        }
    }
}
