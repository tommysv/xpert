﻿using System;
using System.Drawing;
using System.Windows.Forms;

using Xpert.Modell.Objekter.Tipping;

namespace Xpert.Kontroller.Tipping
{
    public partial class UscXpertTippingUtgang : UserControl, IStammeObserverer
    {
        private UscXpertTippingStamme stamme;
        private XpertTippingUtgang data = new XpertTippingUtgang();
        private int antMarkeringer = 0;

        Rectangle[,] pos = new Rectangle[12, 3] {
            {new Rectangle(4, 25, 13, 15), new Rectangle(23, 25, 13, 15), new Rectangle(42, 25, 13, 15)},
            {new Rectangle(4, 46, 13, 15), new Rectangle(23, 46, 13, 15), new Rectangle(42, 46, 13, 15)},
            {new Rectangle(4, 67, 13, 15), new Rectangle(23, 67, 13, 15), new Rectangle(42, 67, 13, 15)},
            {new Rectangle(4, 88, 13, 15), new Rectangle(23, 88, 13, 15), new Rectangle(42, 88, 13, 15)},
            {new Rectangle(4, 109, 13, 15), new Rectangle(23, 109, 13, 15), new Rectangle(42, 109, 13, 15)},
            {new Rectangle(4, 130, 13, 15), new Rectangle(23, 130, 13, 15), new Rectangle(42, 130, 13, 15)},
            {new Rectangle(4, 151, 13, 15), new Rectangle(23, 151, 13, 15), new Rectangle(42, 151, 13, 15)},
            {new Rectangle(4, 172, 13, 15), new Rectangle(23, 172, 13, 15), new Rectangle(42, 172, 13, 15)},
            {new Rectangle(4, 193, 13, 15), new Rectangle(23, 193, 13, 15), new Rectangle(42, 193, 13, 15)},
            {new Rectangle(4, 214, 13, 15), new Rectangle(23, 214, 13, 15), new Rectangle(42, 214, 13, 15)},
            {new Rectangle(4, 235, 13, 15), new Rectangle(23, 235, 13, 15), new Rectangle(42, 235, 13, 15)},
            {new Rectangle(4, 256, 13, 15), new Rectangle(23, 256, 13, 15), new Rectangle(42, 256, 13, 15)}
        };

        Rectangle[] pos_2 = new Rectangle[13] {
            new Rectangle(61, 4, 13, 15), new Rectangle(61, 25, 13, 15), new Rectangle(61, 46, 13, 15),
            new Rectangle(61, 67, 13, 15), new Rectangle(61, 88, 13, 15), new Rectangle(61, 109, 13, 15),
            new Rectangle(61, 130, 13, 15), new Rectangle(61, 151, 13, 15), new Rectangle(61, 172, 13, 15),
            new Rectangle(61, 193, 13, 15), new Rectangle(61, 214, 13, 15), new Rectangle(61, 235, 13, 15),
            new Rectangle(61, 256, 13, 15)
        };

        public UscXpertTippingUtgang(UscXpertTippingStamme stamme)
        {
            InitializeComponent();

            this.stamme = stamme;
        }

        public XpertTippingUtgang Data
        {
            get
            {
                return data;
            }
            set
            {
                data = value;
            }
        }

        public string Beskrivelse
        {
            get
            {
                return lblBeskrivelse.Text;
            }
            set
            {
                lblBeskrivelse.Text = value;
            }
        }

        void IStammeObserverer.StammeMarkeringEndret(int nMarkering)
        {
            data.Markering[nMarkering] &= stamme.Data.Markering[nMarkering];
            antMarkeringer = TellMarkeringer();
            ForkastUgyldigMarkeringTillatt(antMarkeringer);
            Region region = KalkulerOppdaterRegion(nMarkering);
            Invalidate(region);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.antMarkeringer = TellMarkeringer();
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);

            for (int nMarkering = 0; nMarkering < data.Markering.Length; nMarkering++)
            {
                if ((e.Y >= pos[nMarkering, 0].Y) && (e.Y < pos[nMarkering, 0].Y + (pos[nMarkering, 0].Height)))
                {
                    for (int nKolonne = 0; nKolonne < 3; nKolonne++)
                    {
                        if (pos[nMarkering, nKolonne].Contains(e.Location))
                        {
                            Region region = KalkulerOppdaterRegion(nMarkering);
                            switch (nKolonne)
                            {
                                case 0:
                                    if ((stamme.Data.Markering[nMarkering] & 4) == 4)
                                    {
                                        if (data.Markering[nMarkering] == 4)
                                            data.Markering[nMarkering] ^= 4;
                                        else
                                            data.Markering[nMarkering] = 4;
                                        antMarkeringer = TellMarkeringer();
                                        ForkastUgyldigMarkeringTillatt(antMarkeringer);
                                        Invalidate(region);
                                    }
                                    break;
                                case 1:
                                    if ((stamme.Data.Markering[nMarkering] & 2) == 2)
                                    {
                                        if (data.Markering[nMarkering] == 2)
                                            data.Markering[nMarkering] ^= 2;
                                        else
                                            data.Markering[nMarkering] = 2;
                                        antMarkeringer = TellMarkeringer();
                                        ForkastUgyldigMarkeringTillatt(antMarkeringer);
                                        Invalidate(region);
                                    }
                                    break;
                                case 2:
                                    if ((stamme.Data.Markering[nMarkering] & 1) == 1)
                                    {
                                        if (data.Markering[nMarkering] == 1)
                                            data.Markering[nMarkering] ^= 1;
                                        else
                                            data.Markering[nMarkering] = 1;
                                        antMarkeringer = TellMarkeringer();
                                        ForkastUgyldigMarkeringTillatt(antMarkeringer);
                                        Invalidate(region);
                                    }
                                    break;
                            }
                            return;
                        }
                    }
                }
            }

            for (int nMarkeringTillatt = 0; nMarkeringTillatt < 13; nMarkeringTillatt++)
            {
                if (pos_2[nMarkeringTillatt].Contains(e.Location))
                {
                    if (nMarkeringTillatt <= antMarkeringer)
                    {
                        data.MarkeringerTillatt[nMarkeringTillatt] = (data.MarkeringerTillatt[nMarkeringTillatt]) ? false : true;
                        Invalidate(pos_2[nMarkeringTillatt]);
                    }
                    return;
                }
            }
        }

        private int TellMarkeringer()
        {
            int antMarkeringer = 0;
            for (int nMarkering = 0; nMarkering < data.Markering.Length; nMarkering++)
            {
                if (data.Markering[nMarkering] > 0) antMarkeringer++;
            }
            return antMarkeringer;
        }

        private void ForkastUgyldigMarkeringTillatt(int antMarkeringer)
        {
            for (int nMarkeringTillatt = 0; nMarkeringTillatt < 13; nMarkeringTillatt++)
            {
                if (nMarkeringTillatt > antMarkeringer)
                    data.MarkeringerTillatt[nMarkeringTillatt] = false;
            }
        }

        private Region KalkulerOppdaterRegion(int nMarkering)
        {
            Region region = new Region();
            region.Union(pos[nMarkering, 0]);
            region.Union(pos[nMarkering, 1]);
            region.Union(pos[nMarkering, 2]);
            for (int i = 0; i < 13; i++) region.Union(pos_2[i]);
            return region;
        }

        private void TegnMarkeringerTillatt(Graphics graphics, int antMarkeringer, Rectangle clipRectangle)
        {
            using (Font font = new Font("Fixedsys", 7.5f))
            {
                using (StringFormat sf = new StringFormat())
                {
                    sf.Alignment = StringAlignment.Center;
                    sf.LineAlignment = StringAlignment.Center;
                    for (int nMarkering = 0; nMarkering < antMarkeringer + 1; nMarkering++)
                    {
                        if (pos_2[nMarkering].IntersectsWith(clipRectangle))
                        {
                            graphics.FillRectangle(Brushes.White, pos_2[nMarkering]);
                            if (data.MarkeringerTillatt[nMarkering])
                            {
                                graphics.DrawString(nMarkering.ToString(), font, Brushes.Black, (RectangleF)pos_2[nMarkering], sf);
                            }
                        }
                    }
                }
            }
        }

        private void TegnMarkeringer(Graphics graphics, Image kryss, byte[] markeringer, Rectangle clipRectangle)
        {
            for (int nMarkering = 0; nMarkering < markeringer.Length; nMarkering++)
            {
                switch (markeringer[nMarkering])
                {
                    case 1:
                        if (pos[nMarkering, 2].IntersectsWith(clipRectangle))
                            graphics.DrawImage(kryss, pos[nMarkering, 2]);
                        break;
                    case 2:
                        if (pos[nMarkering, 1].IntersectsWith(clipRectangle))
                            graphics.DrawImage(kryss, pos[nMarkering, 1]);
                        break;
                    case 3:
                        if (pos[nMarkering, 1].IntersectsWith(clipRectangle))
                            graphics.DrawImage(kryss, pos[nMarkering, 1]);
                        if (pos[nMarkering, 2].IntersectsWith(clipRectangle))
                            graphics.DrawImage(kryss, pos[nMarkering, 2]);
                        break;
                    case 4:
                        if (pos[nMarkering, 0].IntersectsWith(clipRectangle))
                            graphics.DrawImage(kryss, pos[nMarkering, 0]);
                        break;
                    case 5:
                        if (pos[nMarkering, 0].IntersectsWith(clipRectangle))
                            graphics.DrawImage(kryss, pos[nMarkering, 0]);
                        if (pos[nMarkering, 2].IntersectsWith(clipRectangle))
                            graphics.DrawImage(kryss, pos[nMarkering, 2]);
                        break;
                    case 6:
                        if (pos[nMarkering, 0].IntersectsWith(clipRectangle))
                            graphics.DrawImage(kryss, pos[nMarkering, 0]);
                        if (pos[nMarkering, 1].IntersectsWith(clipRectangle))
                            graphics.DrawImage(kryss, pos[nMarkering, 1]);
                        break;
                    case 7:
                        if (pos[nMarkering, 0].IntersectsWith(clipRectangle))
                            graphics.DrawImage(kryss, pos[nMarkering, 0]);
                        if (pos[nMarkering, 1].IntersectsWith(clipRectangle))
                            graphics.DrawImage(kryss, pos[nMarkering, 1]);
                        if (pos[nMarkering, 2].IntersectsWith(clipRectangle))
                            graphics.DrawImage(kryss, pos[nMarkering, 2]);
                        break;
                }
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            TegnMarkeringer(e.Graphics, Properties.Resources.kryss_2, stamme.Data.Markering, e.ClipRectangle);
            TegnMarkeringerTillatt(e.Graphics, antMarkeringer, e.ClipRectangle);
            TegnMarkeringer(e.Graphics, Properties.Resources.kryss, data.Markering, e.ClipRectangle);
        }
    }
}
