﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

using Xpert.Modell.Objekter.Tipping;

namespace Xpert.Kontroller.Tipping
{
    public partial class UscXpertTippingStamme : UserControl
    {
        private XpertTippingStamme data = new XpertTippingStamme();
        private List<IStammeObserverer> observerer = new List<IStammeObserverer>();

        Rectangle[,] pos = new Rectangle[12, 3] {
{new Rectangle(4, 24, 13, 15), new Rectangle(23, 24, 13, 15), new Rectangle(42, 24, 13, 15)},
{new Rectangle(4, 45, 13, 15), new Rectangle(23, 45, 13, 15), new Rectangle(42, 45, 13, 15)},
{new Rectangle(4, 66, 13, 15), new Rectangle(23, 66, 13, 15), new Rectangle(42, 66, 13, 15)},
{new Rectangle(4, 87, 13, 15), new Rectangle(23, 87, 13, 15), new Rectangle(42, 87, 13, 15)},
{new Rectangle(4, 108, 13, 15), new Rectangle(23, 108, 13, 15), new Rectangle(42, 108, 13, 15)},
{new Rectangle(4, 129, 13, 15), new Rectangle(23, 129, 13, 15), new Rectangle(42, 129, 13, 15)},
{new Rectangle(4, 150, 13, 15), new Rectangle(23, 150, 13, 15), new Rectangle(42, 150, 13, 15)},
{new Rectangle(4, 171, 13, 15), new Rectangle(23, 171, 13, 15), new Rectangle(42, 171, 13, 15)},
{new Rectangle(4, 192, 13, 15), new Rectangle(23, 192, 13, 15), new Rectangle(42, 192, 13, 15)},
{new Rectangle(4, 213, 13, 15), new Rectangle(23, 213, 13, 15), new Rectangle(42, 213, 13, 15)},
{new Rectangle(4, 234, 13, 15), new Rectangle(23, 234, 13, 15), new Rectangle(42, 234, 13, 15)},
{new Rectangle(4, 255, 13, 15), new Rectangle(23, 255, 13, 15), new Rectangle(42, 255, 13, 15)}
        };

        public UscXpertTippingStamme()
        {
            InitializeComponent();
        }

        public XpertTippingStamme Data
        {
            get
            {
                return data;
            }
            set
            {
                data = value;
                Invalidate();
            }
        }

        public void Registrer(IStammeObserverer observerer)
        {
            this.observerer.Add(observerer);
        }

        public void Frigjør(IStammeObserverer observerer)
        {
            this.observerer.Remove(observerer);
        }

        protected void Notifiser(int nMarkering)
        {
            for (int nObserverer = 0; nObserverer < this.observerer.Count; nObserverer++)
                this.observerer[nObserverer].StammeMarkeringEndret(nMarkering);
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);

            for (int nMarkering = 0; nMarkering < data.Markering.Length; nMarkering++)
            {
                if ((e.Y >= pos[nMarkering, 0].Y) && (e.Y < pos[nMarkering, 0].Y + (pos[nMarkering, 0].Height)))
                {
                    for (int nKolonne = 0; nKolonne < 3; nKolonne++)
                    {
                        if (pos[nMarkering, nKolonne].Contains(e.Location))
                        {
                            switch (nKolonne)
                            {
                                case 0: data.Markering[nMarkering] ^= 4;
                                    break;
                                case 1: data.Markering[nMarkering] ^= 2;
                                    break;
                                case 2: data.Markering[nMarkering] ^= 1;
                                    break;
                            }
                            Invalidate(pos[nMarkering, nKolonne]);
                            Notifiser(nMarkering);
                            return;
                        }
                    }
                }
            }
        }

        private void TegnMarkeringer(Graphics graphics, byte[] markeringer, Rectangle clipRectangle)
        {
            for (int nMarkering = 0; nMarkering < markeringer.Length; nMarkering++)
            {
                switch (markeringer[nMarkering])
                {
                    case 1:
                        if (pos[nMarkering, 2].IntersectsWith(clipRectangle))
                            graphics.DrawImage(Properties.Resources.kryss, pos[nMarkering, 2]);
                        break;
                    case 2:
                        if (pos[nMarkering, 1].IntersectsWith(clipRectangle))
                            graphics.DrawImage(Properties.Resources.kryss, pos[nMarkering, 1]);
                        break;
                    case 3:
                        if (pos[nMarkering, 1].IntersectsWith(clipRectangle))
                            graphics.DrawImage(Properties.Resources.kryss, pos[nMarkering, 1]);
                        if (pos[nMarkering, 2].IntersectsWith(clipRectangle))
                            graphics.DrawImage(Properties.Resources.kryss, pos[nMarkering, 2]);
                        break;
                    case 4:
                        if (pos[nMarkering, 0].IntersectsWith(clipRectangle))
                            graphics.DrawImage(Properties.Resources.kryss, pos[nMarkering, 0]);
                        break;
                    case 5:
                        if (pos[nMarkering, 0].IntersectsWith(clipRectangle))
                            graphics.DrawImage(Properties.Resources.kryss, pos[nMarkering, 0]);
                        if (pos[nMarkering, 2].IntersectsWith(clipRectangle))
                            graphics.DrawImage(Properties.Resources.kryss, pos[nMarkering, 2]);
                        break;
                    case 6:
                        if (pos[nMarkering, 0].IntersectsWith(clipRectangle))
                            graphics.DrawImage(Properties.Resources.kryss, pos[nMarkering, 0]);
                        if (pos[nMarkering, 1].IntersectsWith(clipRectangle))
                            graphics.DrawImage(Properties.Resources.kryss, pos[nMarkering, 1]);
                        break;
                    case 7:
                        if (pos[nMarkering, 0].IntersectsWith(clipRectangle))
                            graphics.DrawImage(Properties.Resources.kryss, pos[nMarkering, 0]);
                        if (pos[nMarkering, 1].IntersectsWith(clipRectangle))
                            graphics.DrawImage(Properties.Resources.kryss, pos[nMarkering, 1]);
                        if (pos[nMarkering, 2].IntersectsWith(clipRectangle))
                            graphics.DrawImage(Properties.Resources.kryss, pos[nMarkering, 2]);
                        break;
                }
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            TegnMarkeringer(e.Graphics, data.Markering, e.ClipRectangle);
        }
    }
}
