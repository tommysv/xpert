﻿using System;

namespace Xpert.Kontroller.Tipping
{
    public interface IStammeObserverer
    {
        void StammeMarkeringEndret(int nMarkering);
    }
}
