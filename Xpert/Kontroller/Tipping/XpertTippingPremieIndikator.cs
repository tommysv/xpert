﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Xpert.Kontroller.Tipping
{
    public class XpertTippingPremieIndikator : Control
    {
        private int[] verdier;

        public XpertTippingPremieIndikator()
        {
            verdier = new int[13];
        }

        public int[] Verdier
        {
            get
            {
                return verdier;
            }
        }

        public void Nullstill()
        {
            verdier = new int[13];
        }

        private int FinnMaksVerdi()
        {
            int maksVerdi = 0;
            for (int n = 0; n < verdier.Length; n++)
            {
                if (verdier[n] > maksVerdi)
                {
                    maksVerdi = verdier[n];
                }
            }
            return maksVerdi;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            StringFormat sf_right_aligned = new StringFormat();

            try
            {
                sf_right_aligned
                    .Alignment = StringAlignment.Far;
                sf_right_aligned
                    .LineAlignment = StringAlignment.Far;

                RectangleF rect_0 = new RectangleF();
                rect_0.Location = new PointF(0, 0);
                rect_0.Size = e.Graphics.MeasureString("12. - ", this.Font);
                RectangleF rect_1 = rect_0;
                rect_1.Offset(rect_0.Width, 0);
                rect_1.Size = e.Graphics.MeasureString("531441", this.Font);
                RectangleF rect_2 = rect_1;
                rect_2.Offset(rect_1.Width, 0);
                rect_2.Size = new SizeF(1, ClientRectangle.Height);

                float barX = rect_2.X + 2;
                float barY = rect_2.Y;
                float barHeight = rect_1.Height;
                float maxBarWidth = (float)ClientRectangle.Width - barX; 

                int maksVerdi = FinnMaksVerdi();
                for (int n = verdier.Length - 1; n >= 0; n--)
                {
                    e.Graphics.DrawString(String.Format("{0}. - ", n),
                        this.Font, Brushes.Black, rect_0, sf_right_aligned);
                    e.Graphics.DrawString(verdier[n].ToString(),
                        this.Font, Brushes.Black, rect_1, sf_right_aligned);
                    float valueBarWidth = ((float)verdier[n] / (float)maksVerdi) * maxBarWidth;
                    e.Graphics.FillRectangle(Brushes.Blue, new RectangleF(barX, barY, valueBarWidth, barHeight));
                    rect_0.Offset(0, rect_0.Height + 5);
                    rect_1.Offset(0, rect_1.Height + 5);
                    barY += barHeight + 5;
                }
                e.Graphics.FillRectangle(Brushes.Black, rect_2);
            }
            finally
            {
                sf_right_aligned.Dispose();
            }
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // TipperuIndikator
            // 
            this.Size = new System.Drawing.Size(100, 0);
            this.ResumeLayout(false);

        }
    }
}
