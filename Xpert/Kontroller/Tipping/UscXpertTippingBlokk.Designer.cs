﻿namespace Xpert.Kontroller.Tipping
{
    partial class UscXpertTippingBlokk
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblBeskrivelse = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblBeskrivelse
            // 
            this.lblBeskrivelse.AutoEllipsis = true;
            this.lblBeskrivelse.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblBeskrivelse.Location = new System.Drawing.Point(0, 277);
            this.lblBeskrivelse.Name = "lblBeskrivelse";
            this.lblBeskrivelse.Size = new System.Drawing.Size(59, 13);
            this.lblBeskrivelse.TabIndex = 1;
            this.lblBeskrivelse.Text = "Blokk 1";
            this.lblBeskrivelse.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // UscXpertTippingBlokk
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::Xpert.Properties.Resources.blokk;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Controls.Add(this.lblBeskrivelse);
            this.DoubleBuffered = true;
            this.Name = "UscXpertTippingBlokk";
            this.Size = new System.Drawing.Size(59, 290);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblBeskrivelse;
    }
}
