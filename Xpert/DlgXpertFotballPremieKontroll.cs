﻿using System;
using System.Windows.Forms;


using Xpert.Modell.Objekter.Tipping;

namespace Xpert
{
    public partial class DlgXpertFotballPremieKontroll : Form
    {
        private XpertTippingRekker rekker = new XpertTippingRekker();

        public DlgXpertFotballPremieKontroll()
        {
            InitializeComponent();
        }
        
        public XpertTippingStamme Stamme
        {
            set
            {
                uscTippingResultat.Stamme = value;
            }
        }

        public XpertTippingRekker Rekker
        {
            set
            {
                rekker = value;
            }
        }

        private int TellTegn(XpertTippingRekke premieRekke, XpertTippingRekke rekke)
        {
            int antTegn = 0;
            for (int nMarkering = 0; nMarkering < 12; nMarkering++)
            {
                if (premieRekke.Markering[nMarkering].Equals(rekke.Markering[nMarkering]))
                    antTegn++;
            }
            return antTegn;
        }

        private void btnKontroller_Click(object sender, EventArgs e)
        {
            tippingResultatIndikator.Nullstill();
            for (int nRekke = 0; nRekke < rekker.Count; nRekke++)
            {
                int antTegn = TellTegn(uscTippingResultat.Data, rekker[nRekke]);
                tippingResultatIndikator.Verdier[antTegn] += 1;
            }
            tippingResultatIndikator.Invalidate();
        }

    }
}
