﻿using System;

namespace Xpert.Modell.Objekter
{
    public class XpertException : System.ApplicationException
    {
        public XpertException() : base() { }
        public XpertException(string melding) 
            : base(melding) { }
        public XpertException(string melding, Exception innerException) 
            : base(melding, innerException) { }
    }
}
