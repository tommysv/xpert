﻿using System;

namespace Xpert.Modell.Objekter.Tipping
{
    public class XpertTippingSystem
    {
        private string[,] lag;
        private XpertTippingStamme stamme;
        private XpertTippingBlokker blokker;
        private XpertTippingUtganger utganger;
        private XpertTippingFordelinger tegnFordelinger;
        private XpertTippingMaksRad maksRad;
        private int garantiAnt;
        private bool garantiUtfyllingTilfeldig;

        public XpertTippingSystem()
        {
            stamme = new XpertTippingStamme();
            utganger = new XpertTippingUtganger();
            blokker = new XpertTippingBlokker();
            tegnFordelinger = new XpertTippingFordelinger();
            garantiAnt = 12;
            garantiUtfyllingTilfeldig = true;
            lag = new string[12, 2];
        }

        public string[,] Lag
        {
            get
            {
                return lag;
            }
            set
            {
                lag = value;
            }
        }

        public XpertTippingStamme Stamme
        {
            get
            {
                return stamme;
            }
            set
            {
                stamme = value;
            }
        }

        public XpertTippingBlokker Blokker
        {
            get
            {
                return blokker;
            }
            set
            {
                blokker = value;
            }
        }

        public XpertTippingUtganger Utganger
        {
            get
            {
                return utganger;
            }
            set
            {
                utganger = value;
            }
        }

        public XpertTippingFordelinger TegnFordelinger
        {
            get
            {
                return tegnFordelinger;
            }
            set
            {
                tegnFordelinger = value;
            }
        }

        public XpertTippingMaksRad MaksRad
        {
            get
            {
                return maksRad;
            }
            set
            {
                maksRad = value;
            }
        }

        public int GarantiAnt
        {
            get
            {
                return garantiAnt;
            }
            set
            {
                garantiAnt = value;
            }
        }

        public bool GarantiUtfyllingTilfeldig
        {
            get
            {
                return garantiUtfyllingTilfeldig;
            }
            set
            {
                garantiUtfyllingTilfeldig = value;
            }
        }
    }
}
