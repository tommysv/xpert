﻿using System;

namespace Xpert.Modell.Objekter.Tipping
{
    public class XpertTippingMaksRad
    {
        private int antH = 12;
        private int antU = 12;
        private int antB = 12;

        public int AntH
        {
            get
            {
                return antH;
            }
            set
            {
                antH = value;
            }
        }

        public int AntU
        {
            get
            {
                return antU;
            }
            set
            {
                antU = value;
            }
        }

        public int AntB
        {
            get
            {
                return antB;
            }
            set
            {
                antB = value;
            }
        }
    }
}
