﻿using System;

namespace Xpert.Modell.Objekter.Tipping
{
    public class XpertTippingRekke
    {
        private byte[] markering = new byte[12] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

        public byte[] Markering
        {
            get
            {
                return markering;
            }
            set
            {
                markering = value;
            }
        }
    }
}
