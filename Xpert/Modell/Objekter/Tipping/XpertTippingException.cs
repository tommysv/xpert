﻿using System;

namespace Xpert.Modell.Objekter.Tipping
{
    public class XpertTippingException : System.ApplicationException
    {
        public XpertTippingException() : base() { }
        public XpertTippingException(string melding) 
            : base(melding) { }
        public XpertTippingException(string melding, Exception innerException) 
            : base(melding, innerException) { }
    }
}
