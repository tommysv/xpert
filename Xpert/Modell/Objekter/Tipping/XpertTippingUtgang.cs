﻿using System;

namespace Xpert.Modell.Objekter.Tipping
{
    public class XpertTippingUtgang
    {
        private byte[] markering = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        private bool[] markeringerTillatt = new bool[13] { false, false, false, false, false, false, false, false, false, false, false, false, false };

        public byte[] Markering
        {
            get
            {
                return markering;
            }
            set
            {
                markering = value;
            }
        }

        public bool[] MarkeringerTillatt
        {
            get
            {
                return markeringerTillatt;
            }
            set
            {
                markeringerTillatt = value;
            }
        }
    }
}
