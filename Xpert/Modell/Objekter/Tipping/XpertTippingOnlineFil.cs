﻿using System;
using System.IO;
using System.Text;

namespace Xpert.Modell.Objekter.Tipping
{
    public class XpertTippingOnlineFil
    {
        private string spillerId;
        private string spillerNummer;
        private TippingSpilletid spilletid;
        private TippingTrekningstype trekningType;
        private int antTrekninger;

        public enum TippingSpilletid
        {
            Full,
            Halv,
            Begge
        };

        public enum TippingTrekningstype
        {
            Lørdag,
            Søndag,
            Midtuke
        }

        public XpertTippingOnlineFil()
        {
            this.spillerId = "Uregistrert spiller".PadRight(20, ' ');
            this.spillerNummer = "000000000";
            this.spilletid = TippingSpilletid.Full;
            this.trekningType = TippingTrekningstype.Lørdag;
            this.antTrekninger = 1;
        }

        public string SpillerId
        {
            get { return this.spillerId; }
            set { this.spillerId = value.PadRight(20, ' '); }
        }

        public string SpillerNummer
        {
            get { return this.spillerNummer; }
            set { this.spillerNummer = value; }
        }

        public TippingSpilletid Spilletid
        {
            get { return this.spilletid; }
            set { this.spilletid = value; }
        }

        public TippingTrekningstype TrekningType
        {
            get { return this.trekningType; }
            set { this.trekningType = value; }
        }

        public int AntTrekninger
        {
            get { return this.antTrekninger; }
            set { this.antTrekninger = value; }
        }

        private byte[] KalkulerSjekksum(byte[] data)
        {
            ushort sjekksum = 0;
            byte[] sjekksumBytes = new byte[2];
            for (int i = 0; i < data.Length; i += 2)
            {
                sjekksum += (ushort)(data[i] << 8);
                sjekksum += (ushort)(data[i + 1]);
            }
            sjekksumBytes[0] = (byte)(sjekksum >> 8);
            sjekksumBytes[1] = (byte)sjekksum;
            return sjekksumBytes;
        }

        public void Lagre(string filnavn, XpertTippingRekker rekker)
        {
            if (!Directory.Exists(Path.GetDirectoryName(filnavn)))
                Directory.CreateDirectory(Path.GetDirectoryName(filnavn));

            int antKuponger = rekker.Count / 10 + (((rekker.Count % 10) > 0) ? 1 : 0);

            StringBuilder sb = new StringBuilder();
            using (FileStream fs = File.Create(filnavn, 1024))
            {
                sb.Append("0000");
                sb.Append("003");
                sb.Append(this.spillerId);
                sb.Append("0");
                sb.Append("0");
                sb.Append("00");
                sb.Append("90/00/00");
                sb.Append("00:00:00");
                sb.Append("90/00/00");
                sb.Append("00:00:00");
                sb.Append("0000000000000000");
                sb.Append(antKuponger.ToString().PadLeft(6, '0'));
                sb.Append("000000");
                sb.Append("000000");
                sb.Append("000000");
                sb.Append("000000");
                sb.Append("000000");
                sb.Append("000000");
                sb.Append("000000");
                sb.Append(" ".PadLeft(206, ' '));
                sb.Append("0");
                sb.Append("0");
                sb.Append("TP10");
                sb.Append(this.spillerNummer);
                byte[] header_data = Encoding.ASCII.GetBytes(sb.ToString());
                byte[] header_sjekksum = KalkulerSjekksum(header_data);
                fs.Write(header_data, 0, header_data.Length);
                fs.Write(header_sjekksum, 0, header_sjekksum.Length);

                for (int nKupong = 1; nKupong <= antKuponger; nKupong++)
                {
                    sb.Remove(0, sb.Length);
                    sb.Append(nKupong.ToString().PadLeft(4, '0'));
                    sb.Append("31");

                    switch (spilletid)
                    {
                        case TippingSpilletid.Full: sb.Append("0");
                            break;
                        case TippingSpilletid.Halv: sb.Append("1");
                            break;
                        case TippingSpilletid.Begge: sb.Append("2");
                            break;
                    }

                    switch (trekningType)
                    {
                        case TippingTrekningstype.Lørdag: sb.Append("0");
                            break;
                        case TippingTrekningstype.Søndag: sb.Append("1");
                            break;
                        case TippingTrekningstype.Midtuke: sb.Append("2");
                            break;
                    }

                    int rkIndeks = (nKupong - 1) * 10;
                    int rkKupong = ((rkIndeks + 10) <= rekker.Count) ? 10 : rekker.Count - rkIndeks;
                        
                    sb.Append("00");                                                // Not used
                    sb.Append("01");                                                // Number of draw set to 1
                    sb.Append("0");                                                 // Not used    
                    sb.Append("001");                                               // No of combinations set to 001 for single columns
                    sb.Append(rkKupong.ToString().PadLeft(2, '0'));                 // No of columns filled
                    sb.Append("001");                                                  // Multiplication field set to 1

                    StringBuilder sbKupong = new StringBuilder();
                    for (int nRekke = rkIndeks; nRekke < rkIndeks + rkKupong; nRekke++)
                    {
                        for (int nMarkering = 0; nMarkering < 12; nMarkering++)
                        {
                            switch (rekker[nRekke].Markering[nMarkering])
                            {
                                case 1: sbKupong.Append("02"); break;
                                case 2: sbKupong.Append("03"); break;
                                case 4: sbKupong.Append("01"); break;
                            }
                        }
                        sbKupong.Append("  ");
                    }
                    sb.Append(sbKupong.ToString().PadRight(260, ' '));
                    sb.Append("?????????");         // Not used
                    sb.Append("0000000000000000");  // Not used
                    sb.Append("00000000");          // Not used
                    sb.Append("0");                 // Not used
                    sb.Append("000");               // Not used
                    sb.Append("0");                 // Not used
                    sb.Append("80/01/01");          // Not used
                    sb.Append("00:00:00");          // Not used
                    sb.Append("0000");              // Not used
                    sb.Append("000000000");         // Not used
                    byte[] kupong_data = Encoding.ASCII.GetBytes(sb.ToString());
                    byte[] kupong_sjekksum = KalkulerSjekksum(kupong_data);
                    fs.Write(kupong_data, 0, kupong_data.Length);
                    fs.Write(kupong_sjekksum, 0, kupong_sjekksum.Length);
                }
            }
        }
    }
}
