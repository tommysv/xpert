﻿namespace Xpert
{
    partial class FrmXpertFotballSystem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmXpertFotballSystem));
            this.tsHoved = new System.Windows.Forms.ToolStrip();
            this.tsGenerer = new System.Windows.Forms.ToolStripButton();
            this.tsVisRekker = new System.Windows.Forms.ToolStripButton();
            this.tsPremieKontroll = new System.Windows.Forms.ToolStripButton();
            this.tsKampoppsett = new System.Windows.Forms.ToolStripButton();
            this.tsLagreOnline = new System.Windows.Forms.ToolStripButton();
            this.tsLagreSystem = new System.Windows.Forms.ToolStripButton();
            this.tsLukkSystem = new System.Windows.Forms.ToolStripButton();
            this.ctxHoved = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ctxHovedBlokkMnuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ctxHovedUtgangMnuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ctxHovedTegnfordelingMnuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ctxHovedMaksRadMnuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ctxUtgang = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ctxUtgangSlett = new System.Windows.Forms.ToolStripMenuItem();
            this.ctxTegnFordeling = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ctxTegnFordelingSlett = new System.Windows.Forms.ToolStripMenuItem();
            this.ctxMaksRad = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ctxMaksRadSlett = new System.Windows.Forms.ToolStripMenuItem();
            this.ctxBlokk = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ctxBlokkSlett = new System.Windows.Forms.ToolStripMenuItem();
            this.tsHoved.SuspendLayout();
            this.ctxHoved.SuspendLayout();
            this.ctxUtgang.SuspendLayout();
            this.ctxTegnFordeling.SuspendLayout();
            this.ctxMaksRad.SuspendLayout();
            this.ctxBlokk.SuspendLayout();
            this.SuspendLayout();
            // 
            // tsHoved
            // 
            this.tsHoved.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsGenerer,
            this.tsVisRekker,
            this.tsPremieKontroll,
            this.tsKampoppsett,
            this.tsLagreOnline,
            this.tsLagreSystem,
            this.tsLukkSystem});
            this.tsHoved.Location = new System.Drawing.Point(0, 0);
            this.tsHoved.Name = "tsHoved";
            this.tsHoved.Size = new System.Drawing.Size(593, 25);
            this.tsHoved.TabIndex = 0;
            this.tsHoved.Visible = false;
            // 
            // tsGenerer
            // 
            this.tsGenerer.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsGenerer.Image = ((System.Drawing.Image)(resources.GetObject("tsGenerer.Image")));
            this.tsGenerer.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsGenerer.Name = "tsGenerer";
            this.tsGenerer.Size = new System.Drawing.Size(23, 22);
            this.tsGenerer.Text = "Generer";
            this.tsGenerer.Click += new System.EventHandler(this.tsGenerer_Click);
            // 
            // tsVisRekker
            // 
            this.tsVisRekker.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsVisRekker.Image = ((System.Drawing.Image)(resources.GetObject("tsVisRekker.Image")));
            this.tsVisRekker.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsVisRekker.Name = "tsVisRekker";
            this.tsVisRekker.Size = new System.Drawing.Size(23, 22);
            this.tsVisRekker.Text = "Vis rekker";
            this.tsVisRekker.Click += new System.EventHandler(this.tsVisRekker_Click);
            // 
            // tsPremieKontroll
            // 
            this.tsPremieKontroll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsPremieKontroll.Image = ((System.Drawing.Image)(resources.GetObject("tsPremieKontroll.Image")));
            this.tsPremieKontroll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsPremieKontroll.Name = "tsPremieKontroll";
            this.tsPremieKontroll.Size = new System.Drawing.Size(23, 22);
            this.tsPremieKontroll.Text = "Premiekontroll";
            this.tsPremieKontroll.Click += new System.EventHandler(this.tsPremieKontroll_Click);
            // 
            // tsKampoppsett
            // 
            this.tsKampoppsett.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsKampoppsett.Image = ((System.Drawing.Image)(resources.GetObject("tsKampoppsett.Image")));
            this.tsKampoppsett.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsKampoppsett.Name = "tsKampoppsett";
            this.tsKampoppsett.Size = new System.Drawing.Size(23, 22);
            this.tsKampoppsett.Text = "Kampoppsett";
            this.tsKampoppsett.Click += new System.EventHandler(this.tsKampoppsett_Click);
            // 
            // tsLagreOnline
            // 
            this.tsLagreOnline.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsLagreOnline.Image = ((System.Drawing.Image)(resources.GetObject("tsLagreOnline.Image")));
            this.tsLagreOnline.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsLagreOnline.Name = "tsLagreOnline";
            this.tsLagreOnline.Size = new System.Drawing.Size(23, 22);
            this.tsLagreOnline.Text = "Lagre online";
            this.tsLagreOnline.Click += new System.EventHandler(this.tsLagreOnline_Click);
            // 
            // tsLagreSystem
            // 
            this.tsLagreSystem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsLagreSystem.Image = ((System.Drawing.Image)(resources.GetObject("tsLagreSystem.Image")));
            this.tsLagreSystem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsLagreSystem.Name = "tsLagreSystem";
            this.tsLagreSystem.Size = new System.Drawing.Size(23, 22);
            this.tsLagreSystem.Text = "Lagre";
            this.tsLagreSystem.Click += new System.EventHandler(this.tsLagreSystem_Click);
            // 
            // tsLukkSystem
            // 
            this.tsLukkSystem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsLukkSystem.Image = ((System.Drawing.Image)(resources.GetObject("tsLukkSystem.Image")));
            this.tsLukkSystem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsLukkSystem.Name = "tsLukkSystem";
            this.tsLukkSystem.Size = new System.Drawing.Size(76, 22);
            this.tsLukkSystem.Text = "Lukk system";
            this.tsLukkSystem.Click += new System.EventHandler(this.tsLukkSystem_Click);
            // 
            // ctxHoved
            // 
            this.ctxHoved.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ctxHovedBlokkMnuItem,
            this.ctxHovedUtgangMnuItem,
            this.ctxHovedTegnfordelingMnuItem,
            this.ctxHovedMaksRadMnuItem});
            this.ctxHoved.Name = "ctxHoved";
            this.ctxHoved.ShowImageMargin = false;
            this.ctxHoved.Size = new System.Drawing.Size(125, 92);
            this.ctxHoved.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.ctxHoved_ItemClicked);
            // 
            // ctxHovedBlokkMnuItem
            // 
            this.ctxHovedBlokkMnuItem.Name = "ctxHovedBlokkMnuItem";
            this.ctxHovedBlokkMnuItem.Size = new System.Drawing.Size(124, 22);
            this.ctxHovedBlokkMnuItem.Text = "Blokk";
            // 
            // ctxHovedUtgangMnuItem
            // 
            this.ctxHovedUtgangMnuItem.Name = "ctxHovedUtgangMnuItem";
            this.ctxHovedUtgangMnuItem.Size = new System.Drawing.Size(124, 22);
            this.ctxHovedUtgangMnuItem.Text = "Utgang";
            // 
            // ctxHovedTegnfordelingMnuItem
            // 
            this.ctxHovedTegnfordelingMnuItem.Name = "ctxHovedTegnfordelingMnuItem";
            this.ctxHovedTegnfordelingMnuItem.Size = new System.Drawing.Size(124, 22);
            this.ctxHovedTegnfordelingMnuItem.Text = "Tegnfordeling";
            // 
            // ctxHovedMaksRadMnuItem
            // 
            this.ctxHovedMaksRadMnuItem.Name = "ctxHovedMaksRadMnuItem";
            this.ctxHovedMaksRadMnuItem.Size = new System.Drawing.Size(124, 22);
            this.ctxHovedMaksRadMnuItem.Text = "Maks rad";
            // 
            // ctxUtgang
            // 
            this.ctxUtgang.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ctxUtgangSlett});
            this.ctxUtgang.Name = "ctxUtgang";
            this.ctxUtgang.ShowImageMargin = false;
            this.ctxUtgang.Size = new System.Drawing.Size(73, 26);
            this.ctxUtgang.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.ctxUtgang_ItemClicked);
            // 
            // ctxUtgangSlett
            // 
            this.ctxUtgangSlett.Name = "ctxUtgangSlett";
            this.ctxUtgangSlett.Size = new System.Drawing.Size(72, 22);
            this.ctxUtgangSlett.Text = "Slett";
            // 
            // ctxTegnFordeling
            // 
            this.ctxTegnFordeling.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ctxTegnFordelingSlett});
            this.ctxTegnFordeling.Name = "ctxUtgang";
            this.ctxTegnFordeling.ShowImageMargin = false;
            this.ctxTegnFordeling.Size = new System.Drawing.Size(73, 26);
            this.ctxTegnFordeling.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.ctxTegnFordeling_ItemClicked);
            // 
            // ctxTegnFordelingSlett
            // 
            this.ctxTegnFordelingSlett.Name = "ctxTegnFordelingSlett";
            this.ctxTegnFordelingSlett.Size = new System.Drawing.Size(72, 22);
            this.ctxTegnFordelingSlett.Text = "Slett";
            // 
            // ctxMaksRad
            // 
            this.ctxMaksRad.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ctxMaksRadSlett});
            this.ctxMaksRad.Name = "ctxUtgang";
            this.ctxMaksRad.ShowImageMargin = false;
            this.ctxMaksRad.Size = new System.Drawing.Size(73, 26);
            this.ctxMaksRad.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.ctxMaksRad_ItemClicked);
            // 
            // ctxMaksRadSlett
            // 
            this.ctxMaksRadSlett.Name = "ctxMaksRadSlett";
            this.ctxMaksRadSlett.Size = new System.Drawing.Size(72, 22);
            this.ctxMaksRadSlett.Text = "Slett";
            // 
            // ctxBlokk
            // 
            this.ctxBlokk.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ctxBlokkSlett});
            this.ctxBlokk.Name = "ctxUtgang";
            this.ctxBlokk.ShowImageMargin = false;
            this.ctxBlokk.Size = new System.Drawing.Size(73, 26);
            this.ctxBlokk.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.ctxBlokk_ItemClicked);
            // 
            // ctxBlokkSlett
            // 
            this.ctxBlokkSlett.Name = "ctxBlokkSlett";
            this.ctxBlokkSlett.Size = new System.Drawing.Size(72, 22);
            this.ctxBlokkSlett.Text = "Slett";
            // 
            // FrmXpertFotballSystem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(593, 420);
            this.ContextMenuStrip = this.ctxHoved;
            this.ControlBox = false;
            this.Controls.Add(this.tsHoved);
            this.DoubleBuffered = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmXpertFotballSystem";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.tsHoved.ResumeLayout(false);
            this.tsHoved.PerformLayout();
            this.ctxHoved.ResumeLayout(false);
            this.ctxUtgang.ResumeLayout(false);
            this.ctxTegnFordeling.ResumeLayout(false);
            this.ctxMaksRad.ResumeLayout(false);
            this.ctxBlokk.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip tsHoved;
        private System.Windows.Forms.ToolStripButton tsGenerer;
        private System.Windows.Forms.ToolStripButton tsVisRekker;
        private System.Windows.Forms.ToolStripButton tsPremieKontroll;
        private System.Windows.Forms.ContextMenuStrip ctxHoved;
        private System.Windows.Forms.ToolStripMenuItem ctxHovedUtgangMnuItem;
        private System.Windows.Forms.ToolStripMenuItem ctxHovedTegnfordelingMnuItem;
        private System.Windows.Forms.ToolStripMenuItem ctxHovedMaksRadMnuItem;
        private System.Windows.Forms.ContextMenuStrip ctxUtgang;
        private System.Windows.Forms.ToolStripMenuItem ctxUtgangSlett;
        private System.Windows.Forms.ContextMenuStrip ctxTegnFordeling;
        private System.Windows.Forms.ToolStripMenuItem ctxTegnFordelingSlett;
        private System.Windows.Forms.ContextMenuStrip ctxMaksRad;
        private System.Windows.Forms.ToolStripMenuItem ctxMaksRadSlett;
        private System.Windows.Forms.ToolStripButton tsKampoppsett;
        private System.Windows.Forms.ToolStripMenuItem ctxHovedBlokkMnuItem;
        private System.Windows.Forms.ContextMenuStrip ctxBlokk;
        private System.Windows.Forms.ToolStripMenuItem ctxBlokkSlett;
        private System.Windows.Forms.ToolStripButton tsLagreOnline;
        private System.Windows.Forms.ToolStripButton tsLagreSystem;
        private System.Windows.Forms.ToolStripButton tsLukkSystem;
    }
}