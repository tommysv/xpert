﻿using System;

using Xpert.Visninger;

namespace Xpert.Presenterer
{
    public class Presenterer<T> where T : IXpertVisning
    {
        static Presenterer()
        {
        }

        public Presenterer(T visning)
        {
            Visning = visning;
        }

        protected T Visning { get; private set; }
    }
}
