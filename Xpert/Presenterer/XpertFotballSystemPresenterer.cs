﻿using System;
using System.IO;

using Xpert.Visninger;
using Xpert.Modell.Objekter;
using Xpert.Modell.Objekter.Tipping;

namespace Xpert.Presenterer
{
    public class XpertFotballSystemPresenterer : Presenterer<IXpertFotballSystemVisning>
    {
        public XpertFotballSystemPresenterer(IXpertFotballSystemVisning visning) : base(visning) {}

        public void Vis(string systemfil)
        {
            if (String.IsNullOrEmpty(systemfil))
            {
                Visning.XpertSystem = new XpertTippingSystem();
                Visning.SystemFil = "Uten navn.xpf";
            }
            else
            {
                Visning.XpertSystem = XpertTippingSystemFil.Hent(systemfil);
                Visning.SystemFil = systemfil;
            }
        }

        public void Generer(XpertTippingSystem system)
        {
            Visning.Rekker = XpertTippingSystemGenerator.Generer(system);       
        }

        public void LagreSystem(string filnavn, XpertTippingSystem system)
        {
            XpertTippingSystemFil.Lagre(filnavn, system);
            Visning.XpertSystem = system;
            Visning.SystemFil = filnavn;
        }

        public void LagreOnline(string filnavn, string spillerId, string spillerNummer, XpertTippingOnlineFil.TippingTrekningstype trekningType, XpertTippingOnlineFil.TippingSpilletid spilleTid, XpertTippingRekker rekker)
        {
            XpertTippingOnlineFil onlineFil = new XpertTippingOnlineFil();
            onlineFil.SpillerId = spillerId;
            onlineFil.SpillerNummer = spillerNummer;
            onlineFil.Spilletid = spilleTid;
            onlineFil.TrekningType = trekningType;
            onlineFil.AntTrekninger = 1;
            onlineFil.Lagre(filnavn, rekker);
        }
    }
}
