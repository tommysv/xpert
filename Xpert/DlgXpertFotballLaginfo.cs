﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows.Forms;

using Xpert.Presenterer;
using Xpert.Visninger;

namespace Xpert
{
    public partial class DlgXpertFotballLaginfo : Form, IXpertFotballLaginfoVisning
    {
        private XpertFotballLaginfoPresenterer presenterer;
        private Dictionary<string, ArrayList> lagInfo;
        private TextBox[] tekstbokser;
        private int fokusIndex;

        public DlgXpertFotballLaginfo()
        {
            InitializeComponent();
            presenterer = new XpertFotballLaginfoPresenterer(this);
            tekstbokser = new TextBox[] { 
                txtLagH1, txtLagB1, txtLagH2, txtLagB2, txtLagH3, txtLagB3, 
                txtLagH4, txtLagB4, txtLagH5, txtLagB5, txtLagH6, txtLagB6,
                txtLagH7, txtLagB7, txtLagH8, txtLagB8, txtLagH9, txtLagB9, 
                txtLagH10, txtLagB10, txtLagH11, txtLagB11, txtLagH12, txtLagB12
            };
        }

        public string[,] Lag
        {
            get
            {
                string[,] lag = new string[12, 2];
                for (int n = 0; n < 12; n++)
                {
                    lag[n, 0] = tekstbokser[(n << 1)].Text;
                    lag[n, 1] = tekstbokser[(n << 1) + 1].Text;
                }
                return lag;
            }
            set
            {
                for (int n = 0; n < 12; n++)
                {
                    tekstbokser[(n << 1)].Text = value[n, 0];
                    tekstbokser[(n << 1) + 1].Text = value[n, 1];
                }
            }
        }

        public Dictionary<string, ArrayList> LagInfo
        {
            set
            {
                if (value != null)
                {
                    foreach (KeyValuePair<string, ArrayList> land in value)
                    {
                        cbxLand.Items.Add(land.Key);
                    }
                    lagInfo = value;

                    if (value.Keys.Count > 0)
                        cbxLand.SelectedIndex = 0;
                }
            }
        }

        private void FlyttFokus(int index)
        {
            TextBox textBox = tekstbokser[index];
            textBox.Focus();
            fokusIndex = index;
        }

        private void SettFokusIndeks(TextBox textBox)
        {
            for (int n = 0; n < tekstbokser.Length; n++)
            {
                if (tekstbokser[n].Equals(textBox))
                {
                    fokusIndex = n;
                    break;
                }
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);    
            presenterer.Vis();
            FlyttFokus(0);
        }

        private void cbxLand_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbxLand.SelectedIndex != -1)
            {
                lbxLag.Items.Clear();
                string land = cbxLand.Text;
                ArrayList lag = lagInfo[land];
                for (int nLag = 0; nLag < lag.Count; nLag++)
                {
                    lbxLag.Items.Add(lag[nLag]);
                }
            }
        }

        private void lbxLag_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int itemIndex = lbxLag.IndexFromPoint(e.Location);
            if (itemIndex != ListBox.NoMatches)
            {
                tekstbokser[fokusIndex].Text = Convert.ToString(lbxLag.Items[itemIndex]);
                if (fokusIndex == 23)
                    fokusIndex = 0;
                else
                    fokusIndex++;
                FlyttFokus(fokusIndex);
            }
        }

        private void txtLagH1_Enter(object sender, EventArgs e)
        {
            SettFokusIndeks(txtLagH1);
        }

        private void txtLagH2_Enter(object sender, EventArgs e)
        {
            SettFokusIndeks(txtLagH2);
        }

        private void txtLagH3_Enter(object sender, EventArgs e)
        {
            SettFokusIndeks(txtLagH3);
        }

        private void txtLagH4_Enter(object sender, EventArgs e)
        {
            SettFokusIndeks(txtLagH4);
        }

        private void txtLagH5_Enter(object sender, EventArgs e)
        {
            SettFokusIndeks(txtLagH5);
        }

        private void txtLagH6_Enter(object sender, EventArgs e)
        {
            SettFokusIndeks(txtLagH6);
        }

        private void txtLagH7_Enter(object sender, EventArgs e)
        {
            SettFokusIndeks(txtLagH7);
        }

        private void txtLagH8_Enter(object sender, EventArgs e)
        {
            SettFokusIndeks(txtLagH8);
        }

        private void txtLagH9_Enter(object sender, EventArgs e)
        {
            SettFokusIndeks(txtLagH9);
        }

        private void txtLagH10_Enter(object sender, EventArgs e)
        {
            SettFokusIndeks(txtLagH10);
        }

        private void txtLagH11_Enter(object sender, EventArgs e)
        {
            SettFokusIndeks(txtLagH11);
        }

        private void txtLagH12_Enter(object sender, EventArgs e)
        {
            SettFokusIndeks(txtLagH12);
        }

        private void txtLagB1_Enter(object sender, EventArgs e)
        {
            SettFokusIndeks(txtLagB1);
        }

        private void txtLagB2_Enter(object sender, EventArgs e)
        {
            SettFokusIndeks(txtLagB2);
        }

        private void txtLagB3_Enter(object sender, EventArgs e)
        {
            SettFokusIndeks(txtLagB3);
        }

        private void txtLagB4_Enter(object sender, EventArgs e)
        {
            SettFokusIndeks(txtLagB4);
        }

        private void txtLagB5_Enter(object sender, EventArgs e)
        {
            SettFokusIndeks(txtLagB5);
        }

        private void txtLagB6_Enter(object sender, EventArgs e)
        {
            SettFokusIndeks(txtLagB6);
        }

        private void txtLagB7_Enter(object sender, EventArgs e)
        {
            SettFokusIndeks(txtLagB7);
        }

        private void txtLagB8_Enter(object sender, EventArgs e)
        {
            SettFokusIndeks(txtLagB8);
        }

        private void txtLagB9_Enter(object sender, EventArgs e)
        {
            SettFokusIndeks(txtLagB9);
        }

        private void txtLagB10_Enter(object sender, EventArgs e)
        {
            SettFokusIndeks(txtLagB10);
        }

        private void txtLagB11_Enter(object sender, EventArgs e)
        {
            SettFokusIndeks(txtLagB11);
        }

        private void txtLagB12_Enter(object sender, EventArgs e)
        {
            SettFokusIndeks(txtLagB12);
        }
    }
}
