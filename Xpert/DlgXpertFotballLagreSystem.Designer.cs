﻿namespace Xpert
{
    partial class DlgXpertFotballLagreSystem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnLagre = new System.Windows.Forms.Button();
            this.lbxSystemer = new System.Windows.Forms.ListBox();
            this.lblKatalog = new System.Windows.Forms.Label();
            this.txtFilnavn = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnLagre
            // 
            this.btnLagre.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnLagre.Location = new System.Drawing.Point(205, 217);
            this.btnLagre.Name = "btnLagre";
            this.btnLagre.Size = new System.Drawing.Size(75, 23);
            this.btnLagre.TabIndex = 0;
            this.btnLagre.Text = "Lagre";
            this.btnLagre.UseVisualStyleBackColor = true;
            // 
            // lbxSystemer
            // 
            this.lbxSystemer.FormattingEnabled = true;
            this.lbxSystemer.Location = new System.Drawing.Point(6, 27);
            this.lbxSystemer.Name = "lbxSystemer";
            this.lbxSystemer.Size = new System.Drawing.Size(274, 186);
            this.lbxSystemer.TabIndex = 1;
            this.lbxSystemer.SelectedIndexChanged += new System.EventHandler(this.lbxSystemer_SelectedIndexChanged);
            // 
            // lblKatalog
            // 
            this.lblKatalog.AutoEllipsis = true;
            this.lblKatalog.Location = new System.Drawing.Point(3, 9);
            this.lblKatalog.Name = "lblKatalog";
            this.lblKatalog.Size = new System.Drawing.Size(277, 13);
            this.lblKatalog.TabIndex = 2;
            // 
            // txtFilnavn
            // 
            this.txtFilnavn.Location = new System.Drawing.Point(6, 219);
            this.txtFilnavn.Name = "txtFilnavn";
            this.txtFilnavn.Size = new System.Drawing.Size(177, 20);
            this.txtFilnavn.TabIndex = 3;
            // 
            // DlgXpertTippingLagreSystem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 244);
            this.Controls.Add(this.txtFilnavn);
            this.Controls.Add(this.lblKatalog);
            this.Controls.Add(this.lbxSystemer);
            this.Controls.Add(this.btnLagre);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DlgXpertTippingLagreSystem";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Lagre system";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnLagre;
        private System.Windows.Forms.ListBox lbxSystemer;
        private System.Windows.Forms.Label lblKatalog;
        private System.Windows.Forms.TextBox txtFilnavn;
    }
}