﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Xpert.Kontroller.Tipping;
using Xpert.Presenterer;
using Xpert.Visninger;
using Xpert.Modell.Objekter.Tipping;

namespace Xpert
{
    public partial class FrmXpertFotballSystem : Form, IXpertFotballSystemVisning
    {
        private string systemFil;
        private XpertTippingSystem system = null;
        private XpertTippingRekker rekker = null;
        private UscXpertTippingLaginfo lagInfo;
        private UscXpertTippingStamme stamme;
        private List<UscXpertTippingBlokk> blokker;
        private List<UscXpertTippingUtgang> utganger;
        private List<UscXpertTippingTegnFordeling> tegnfordelinger;
        private UscXpertTippingMaksRad maksRad;
        private XpertFotballSystemPresenterer presenter;

        public FrmXpertFotballSystem()
        {
            InitializeComponent();

            presenter = new XpertFotballSystemPresenterer(this);
        }

        public ToolStrip ToolStrip
        {
            get { return tsHoved; }
        }

        public string SystemFil
        {
            get
            {
                return systemFil;
            }
            set
            {
                this.Text = (String.IsNullOrEmpty(value)) ? "uten navn.xpf" : value;
                systemFil = value;
            }
        }

        public XpertTippingSystem XpertSystem
        {
            set
            {
                InitialiserKontroller(value);
                OppdaterKontroller();
                this.system = value;
            }
        }

        public XpertTippingRekker Rekker
        {
            set
            {
                rekker = value;
            }
        }

        #region Private Methods
        private void InitialiserKontroller(XpertTippingSystem system)
        {
            this.lagInfo = new UscXpertTippingLaginfo();
            lagInfo.Visible = false;
            lagInfo.Lag = system.Lag;
            this.Controls.Add(lagInfo);

            this.stamme = new UscXpertTippingStamme();
            stamme.Visible = false;
            stamme.Data = system.Stamme;
            this.Controls.Add(stamme);

            this.blokker = new List<UscXpertTippingBlokk>();
            for (int nBlokk = 0; nBlokk < system.Blokker.Count; nBlokk++)
            {
                UscXpertTippingBlokk blokk = new UscXpertTippingBlokk();
                blokk.ContextMenuStrip = ctxBlokk;
                blokk.Data = system.Blokker[nBlokk];
                blokk.Beskrivelse = String.Format("Blokk {0}", nBlokk + 1);
                blokk.Visible = false;
                this.blokker.Add(blokk);
            }
            this.Controls.AddRange(this.blokker.ToArray());

            this.utganger = new List<UscXpertTippingUtgang>();
            for (int nUtgang = 0; nUtgang < system.Utganger.Count; nUtgang++)
            {
                UscXpertTippingUtgang utgang = new UscXpertTippingUtgang(stamme);
                utgang.ContextMenuStrip = ctxUtgang;
                utgang.Data = system.Utganger[nUtgang];
                utgang.Beskrivelse = String.Format("Utgang {0}", nUtgang + 1);
                utgang.Visible = false;
                this.utganger.Add(utgang);
                stamme.Registrer(utgang);
            }
            this.Controls.AddRange(this.utganger.ToArray());

            this.tegnfordelinger = new List<UscXpertTippingTegnFordeling>();
            for (int nTegnFordeling = 0; nTegnFordeling < system.TegnFordelinger.Count; nTegnFordeling++)
            {
                UscXpertTippingTegnFordeling tegnFordeling = new UscXpertTippingTegnFordeling();
                tegnFordeling.ContextMenuStrip = ctxTegnFordeling;
                tegnFordeling.Data = system.TegnFordelinger[nTegnFordeling];
                tegnFordeling.Beskrivelse = String.Format("Tegnfordeling {0}", nTegnFordeling + 1);
                tegnFordeling.Visible = false;
                this.tegnfordelinger.Add(tegnFordeling);
            }
            this.Controls.AddRange(this.tegnfordelinger.ToArray());

            if (system.MaksRad != null)
            {
                maksRad = new UscXpertTippingMaksRad();
                maksRad.ContextMenuStrip = ctxMaksRad;
                maksRad.Data = system.MaksRad;
                maksRad.Visible = false;
                this.Controls.Add(maksRad);
            }
        }

        private void OppdaterKontroller()
        {
            Point pos = new Point(10, 50);
            lagInfo.Location = pos;
            if (!lagInfo.Visible) lagInfo.Visible = true;

            pos.X += lagInfo.Width + 10;
            stamme.Location = pos;
            if (!stamme.Visible) stamme.Visible = true;

            pos.X += stamme.Width + 10;
            for (int nBlokk = 0; nBlokk < blokker.Count; nBlokk++)
            {
                blokker[nBlokk].Location = pos;
                blokker[nBlokk].Beskrivelse = String.Format("Blokk {0}", nBlokk + 1);
                if (!blokker[nBlokk].Visible) blokker[nBlokk].Visible = true;
                pos.X += blokker[nBlokk].Width + 10;
            }
            pos.Y -= 1;
            for (int nUtgang = 0; nUtgang < utganger.Count; nUtgang++)
            {
                utganger[nUtgang].Location = pos;
                utganger[nUtgang].Beskrivelse = String.Format("Utgang {0}", nUtgang + 1);
                if (!utganger[nUtgang].Visible) utganger[nUtgang].Visible = true;
                pos.X += utganger[nUtgang].Width + 10;
            }

            pos.Y -= 20;
            for (int nTegnFordeling = 0; nTegnFordeling < tegnfordelinger.Count; nTegnFordeling++)
            {
                tegnfordelinger[nTegnFordeling].Location = pos;
                tegnfordelinger[nTegnFordeling].Beskrivelse = String.Format("Tegnfordeling {0}", nTegnFordeling + 1);
                if (!tegnfordelinger[nTegnFordeling].Visible) tegnfordelinger[nTegnFordeling].Visible = true;
                pos.X += tegnfordelinger[nTegnFordeling].Width + 10;
            }

            pos.Y += 21;
            if (maksRad != null)
            {
                maksRad.Location = pos;
                if (!maksRad.Visible) maksRad.Visible = true;
            }
        }
        #endregion

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            presenter.Vis(systemFil);
        }

        private void ctxHoved_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ToolStripMenuItem mnuItem = e.ClickedItem as ToolStripMenuItem;
            switch (mnuItem.Name)
            {
                case "ctxHovedBlokkMnuItem":
                    UscXpertTippingBlokk blokk = new UscXpertTippingBlokk();
                    blokk.ContextMenuStrip = ctxBlokk;
                    blokk.Visible = false;
                    blokk.Data = new XpertTippingBlokk();
                    this.blokker.Add(blokk);
                    this.Controls.Add(blokk);
                    system.Blokker.Add(blokk.Data);
                    OppdaterKontroller();
                    break;
                case "ctxHovedUtgangMnuItem":
                    UscXpertTippingUtgang utgang = new UscXpertTippingUtgang(stamme);
                    utgang.ContextMenuStrip = ctxUtgang;
                    utgang.Visible = false;
                    utgang.Data = new XpertTippingUtgang();
                    this.stamme.Registrer(utgang);
                    this.utganger.Add(utgang);
                    this.Controls.Add(utgang);
                    system.Utganger.Add(utgang.Data);
                    OppdaterKontroller();
                    break;
                case "ctxHovedTegnfordelingMnuItem":
                    UscXpertTippingTegnFordeling tegnFordeling = new UscXpertTippingTegnFordeling();
                    tegnFordeling.ContextMenuStrip = ctxTegnFordeling;
                    tegnFordeling.Visible = false;
                    tegnFordeling.Data = new XpertTippingFordeling();
                    this.tegnfordelinger.Add(tegnFordeling);
                    this.Controls.Add(tegnFordeling);
                    system.TegnFordelinger.Add(tegnFordeling.Data);
                    OppdaterKontroller();
                    break;
                case "ctxHovedMaksRadMnuItem":
                    if (maksRad == null)
                    {
                        maksRad = new UscXpertTippingMaksRad();
                        maksRad.ContextMenuStrip = ctxMaksRad;
                        maksRad.Visible = false;
                        maksRad.Data = new XpertTippingMaksRad();
                        this.Controls.Add(maksRad);
                        system.MaksRad = maksRad.Data;
                        OppdaterKontroller();
                    }
                    break;
                default:
                    break;
            }
        }


        private void ctxBlokk_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ToolStripMenuItem mnuItem = e.ClickedItem as ToolStripMenuItem;
            switch (mnuItem.Name)
            {
                case "ctxBlokkSlett":
                    ContextMenuStrip mnuStrip = sender as ContextMenuStrip;
                    UscXpertTippingBlokk blokk = (UscXpertTippingBlokk)mnuStrip.SourceControl;
                    this.blokker.Remove(blokk);
                    this.Controls.Remove(blokk);
                    system.Blokker.Remove(blokk.Data);
                    blokk.Dispose();
                    OppdaterKontroller();
                    break;
            }
        }

        private void ctxUtgang_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ToolStripMenuItem mnuItem = e.ClickedItem as ToolStripMenuItem;
            switch (mnuItem.Name)
            {
                case "ctxUtgangSlett":
                    ContextMenuStrip mnuStrip = sender as ContextMenuStrip;
                    UscXpertTippingUtgang utgang = (UscXpertTippingUtgang)mnuStrip.SourceControl;
                    this.stamme.Frigjør(utgang);
                    this.utganger.Remove(utgang);
                    this.Controls.Remove(utgang);
                    system.Utganger.Remove(utgang.Data);
                    utgang.Dispose();
                    OppdaterKontroller();
                    break;
            }
        }

        private void ctxTegnFordeling_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ToolStripMenuItem mnuItem = e.ClickedItem as ToolStripMenuItem;
            switch (mnuItem.Name)
            {
                case "ctxTegnFordelingSlett":
                    ContextMenuStrip mnuStrip = sender as ContextMenuStrip;
                    UscXpertTippingTegnFordeling tegnfordeling = (UscXpertTippingTegnFordeling)mnuStrip.SourceControl;
                    this.tegnfordelinger.Remove(tegnfordeling);
                    this.Controls.Remove(tegnfordeling);
                    system.TegnFordelinger.Remove(tegnfordeling.Data);
                    tegnfordeling.Dispose();
                    OppdaterKontroller();
                    break;
            }
        }

        private void ctxMaksRad_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ToolStripMenuItem mnuItem = e.ClickedItem as ToolStripMenuItem;
            switch (mnuItem.Name)
            {
                case "ctxMaksRadSlett":
                    ContextMenuStrip mnuStrip = sender as ContextMenuStrip;
                    UscXpertTippingMaksRad maksRad = (UscXpertTippingMaksRad)mnuStrip.SourceControl;

                    this.maksRad = null;
                    this.Controls.Remove(maksRad);
                    system.MaksRad = null;
                    maksRad.Dispose();
                    OppdaterKontroller();
                    break;
            }
        }

        private void tsVisRekker_Click(object sender, EventArgs e)
        {
            DlgXpertFotballRekker dlgRekker = new DlgXpertFotballRekker();
            dlgRekker.Rekker = rekker;
            dlgRekker.ShowDialog();
        }

        private void tsGenerer_Click(object sender, EventArgs e)
        {
            presenter.Generer(system);
            DlgMessageBox messageBox = new DlgMessageBox();
            messageBox.Caption = "Xpert Systemtipping";
            messageBox.Message = String.Format("{0} rekker generert !", rekker.Count);
            messageBox.ShowDialog();
        }

        private void tsKampoppsett_Click(object sender, EventArgs e)
        {
            DlgXpertFotballLaginfo dlgLagInfo = new DlgXpertFotballLaginfo();
            dlgLagInfo.Lag = system.Lag;
            DialogResult result = dlgLagInfo.ShowDialog();
            if (result == DialogResult.OK)
            {
                system.Lag = dlgLagInfo.Lag;
                lagInfo.Visible = false;
                lagInfo.Lag = dlgLagInfo.Lag;
                OppdaterKontroller();
            }
        }

        private void tsLagreOnline_Click(object sender, EventArgs e)
        {
            DlgXpertFotballLagreOnline dlgLagreOnline = new DlgXpertFotballLagreOnline();
            DialogResult result = dlgLagreOnline.ShowDialog();
            if (result == DialogResult.OK)
            {
                string filnavn = dlgLagreOnline.Filnavn;
                string spillerId = dlgLagreOnline.SpillerId;
                string spillerNummer = dlgLagreOnline.SpillerNummer;
                XpertTippingOnlineFil.TippingTrekningstype trekningType = dlgLagreOnline.TrekningType;
                XpertTippingOnlineFil.TippingSpilletid spilleTid = dlgLagreOnline.Spilletid;
                presenter.LagreOnline(filnavn, spillerId, spillerNummer, trekningType, spilleTid, rekker);
            }
        }

        private void tsPremieKontroll_Click(object sender, EventArgs e)
        {
            DlgXpertFotballPremieKontroll dlgPremieKontroll = new DlgXpertFotballPremieKontroll();
            dlgPremieKontroll.Stamme = system.Stamme;
            dlgPremieKontroll.Rekker = rekker;
            dlgPremieKontroll.ShowDialog();
        }

        private void tsLagreSystem_Click(object sender, EventArgs e)
        {
            DlgXpertFotballLagreSystem dlgLagreSystem = new DlgXpertFotballLagreSystem();
            DialogResult result = dlgLagreSystem.ShowDialog();
            if (result == DialogResult.OK)
            {
                presenter.LagreSystem(dlgLagreSystem.Filnavn, system);
            }
        }

        private void tsLukkSystem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
